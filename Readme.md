# Ambiente de construção Java
Conta com ferramentas como:

* Java 11 (OpenJDK)

* Maven 3.x

* NodeJS 14.x

* pnpm/yarn/npm

* OpenJFX (JavaFX)

* awscli (AWS CLI)

* gcloud (GCE CLI)

* firebase (Firebase CLI)
 
