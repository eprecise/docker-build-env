#/bin/bash

apt update
apt install curl apt-utils apt-transport-https tar bash ca-certificates openssl unzip -y

# Gcloud CLI
echo "deb http://packages.cloud.google.com/apt cloud-sdk-stretch main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

# Yarn CLI
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | tee /usr/share/keyrings/yarnkey.gpg >/dev/null
echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list

# Node/NPM
curl -sL https://deb.nodesource.com/setup_14.x | bash -

apt remove cmdtest -y
apt update
apt install python3-pip nodejs yarn openjfx google-cloud-sdk  -y

pip install awscli
yarn global add firebase-tools
yarn global add -g pnpm

sed -i "/<profiles>/ a\
<profile>\
  <id>eprecise</id>\
  <activation>\
    <activeByDefault>true</activeByDefault>\
  </activation>\
  <repositories>\
    <repository>\
      <id>eprecise-all</id>\
      <url>https://nexus.e-precise.com.br/repository/all/</url>\
    </repository>\
  </repositories>\
</profile>" /usr/share/maven/conf/settings.xml

rm -rf /var/cache/apt/*
