FROM maven:3.6-ibmjava-8-alpine

COPY prepareEnv.sh /opt/prepareEnv.sh

RUN bash -x /opt/prepareEnv.sh
